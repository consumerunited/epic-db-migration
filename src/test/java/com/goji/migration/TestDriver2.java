/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.goji.common.value.Gender;
import com.goji.common.value.MaritalStatus;
import com.goji.orm.model.epic.EpicClient;
import com.goji.orm.model.epic.EpicLine;
import com.goji.orm.model.epic.EpicPolicyType;
import com.goji.orm.spring.converter.Converter;
import com.goji.orm.spring.dao.CarrierDAO;
import com.goji.orm.spring.dao.CustomerDAO;
import com.goji.orm.spring.dao.DAO;
import com.goji.orm.spring.dao.EpicClientDAO;
import com.goji.orm.spring.dao.EpicLineDAO;
import com.goji.orm.spring.dao.ExternalSystemDAO;
import com.goji.orm.spring.dao.PersonDAO;
import com.goji.orm.spring.dao.PolicyDAO;
import com.goji.orm.spring.dao.PolicyTypeDAO;
import com.goji.orm.spring.dao.StateProvinceDAO;
import com.goji.orm.spring.domain.Customer;
import com.goji.orm.spring.domain.CustomerExternalId;
import com.goji.orm.spring.domain.Person;
import com.goji.orm.spring.domain.PersonEmail;
import com.goji.orm.spring.domain.Policy;
import com.goji.orm.spring.domain.PolicyExternalId;
import com.goji.orm.spring.value.PolicyStatus;
import io.dropwizard.logging.BootstrapLogging;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        loader = AnnotationConfigContextLoader.class,
        classes = TestConfig.class
)
@ActiveProfiles("local")
public class TestDriver2 {

    @Autowired
    PolicyDAO policyDAO;

    @Autowired
    CustomerDAO customerDAO;

    @Autowired
    PersonDAO personDAO;

    @Autowired
    EpicClientDAO clientDAO;

    @Autowired
    EpicLineDAO lineDAO;

    @Autowired
    DAO<CustomerExternalId> customerExternalIdDAO;

    @Autowired
    private Converter<EpicClient, Customer> customerConverter;

    @Autowired
    private Converter<EpicLine, Policy> policyConverter;

    @Autowired
    CarrierDAO carrierDAO;

    @Autowired
    StateProvinceDAO stateDAO;

    @Autowired
    ExternalSystemDAO externalSystemDAO;

    @Autowired
    PolicyTypeDAO policyTypeDAO;

    @Autowired
    private TransactionTemplate tt;

    Logger logger = LoggerFactory.getLogger(TestDriverETL.class);

    @BeforeClass
    public static void beforeClass() {

        BootstrapLogging.bootstrap(Level.INFO);
        ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("com.goji").setLevel(Level.TRACE);
    }

    @Test
    public void testDriverMerge() {
        createOrUpdateTestCustomer();
        createOrUpdateTestPolicy();
    }

    private void createOrUpdateTestCustomer() {
        tt.execute(t -> {
//            EpicClient client = clientDAO.get("436005");
//            Customer c = customerConverter.convert(client);
            Customer c = new Customer();
            Person primaryContact = new Person();
            primaryContact.setFirstName("Evelyn");
            primaryContact.setLastName("Agyare");
            primaryContact.setEmail(new PersonEmail(primaryContact, "thetestemail@goji.com"));
            c.setPrimaryContact(primaryContact);

            Set<CustomerExternalId> custExternalIds = new HashSet<>();
            custExternalIds.add(new CustomerExternalId("EPIC", "436005"));
            c.setExternalIds(custExternalIds);
            
            customerDAO.hydrate(c);
            return customerDAO.update(c);
        });
    }

    private void createOrUpdateTestPolicy() {
        tt.execute(t -> {
//            EpicLine l = lineDAO.get("892106");

            Policy p =  getATestPolicy();// policyConverter.convert(l);
            policyDAO.hydrate(p);
            return policyDAO.update(p);
        });
    }
    
    
    private Policy getATestPolicy() {
        Policy policy = new Policy();
        policy.setPolicyNumber("HPA00001171829");
        policy.setCarrier(carrierDAO.load("PLMRCK"));
        policy.setIssuingState(stateDAO.load("NJ"));
        policy.setType(policyTypeDAO.load("AUTO"));
        policy.setTerm(Months.SIX);
        policy.setStatus(PolicyStatus.CONFIRMED);
        policy.setPremium(new BigDecimal(915));
        policy.setFirstEffectiveDate(new LocalDate(2015, 10, 27));

        policy.setEffectiveDate(new LocalDate(2016, 10, 27));
        policy.setExpirationDate(new LocalDate(2017, 10, 27));
//        policy.setBillToAddress(billToAddress);

        Set<PolicyExternalId> externalIds = new HashSet<>();
        externalIds.add(new PolicyExternalId("EPIC", "892106"));
        policy.setExternalIds(externalIds);

        Customer cust = new Customer();
        Set<CustomerExternalId> custExternalIds = new HashSet<>();
        custExternalIds.add(new CustomerExternalId("EPIC", "436005"));
        cust.setExternalIds(custExternalIds);
        policy.setCustomer(cust);

        Person driver = new Person();
        driver.setFirstName("Evelyn");
        driver.setLastName("Agyare");
        driver.setGender(Gender.F);
        driver.setDateOfBirth(new LocalDate(1980, 8, 20));
        driver.setMaritalStatus(MaritalStatus.SINGLE);
        driver.setLicenseNumber("A31222540058802");
        driver.setLicenseState(stateDAO.load("NJ"));
        driver.setDateFirstLicensed(new LocalDate(2000, 1, 1));
        policy.setDrivers(Arrays.asList(driver));
        
        return policy;
    }
}
