package com.goji.migration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author yinzara
 */
@Configuration
@ComponentScan("com.goji")
public class TestConfig {
    
}
