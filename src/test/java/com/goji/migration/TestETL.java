package com.goji.migration;

import com.goji.orm.model.epic.EpicClient;
import com.goji.orm.spring.converter.Converter;
import com.goji.orm.spring.dao.CustomerDAO;
import com.goji.orm.spring.dao.EpicClientDAO;
import com.goji.orm.spring.dao.PersonDAO;
import com.goji.orm.spring.domain.Customer;
import com.goji.orm.spring.domain.Person;
import com.goji.orm.spring.value.ExternalSystemCode;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author pcm
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(
//        loader = AnnotationConfigContextLoader.class,
//        classes = TestConfig.class
//)
//@ActiveProfiles("stage")
public class TestETL {

    @Autowired
    EpicClientDAO clientDAO;

    @Autowired
    CustomerDAO gojiCustomerDAO;

    @Autowired
    PersonDAO gojiPersonDAO;

    @Autowired
    private Converter<EpicClient, Customer> customerConverter;

    @Autowired
    private TransactionTemplate transaction;

    // Test Customer -- Kathy Manhollan (UniqEntity - 143920)
//    @Test
    public void testCustomer() {

        EpicClient epicClient
                = transaction.execute((t) -> {
                    return clientDAO.get(143920);
                });
        Customer cust = transaction.execute((t) -> {
            Customer customer = customerConverter.convert(epicClient);
            Customer existing = findExistingCustomer(customer);
            if (existing != null) {
                System.out.println("\t --------- Found existingCustomer --------- ");
                customer.setCustomerId(existing.getCustomerId());
                if (existing.getPrimaryContact() != null) {
                    final Person primaryContact = customer.getPrimaryContact();
                    primaryContact.setPersonId(existing.getPrimaryContact().getPersonId());
                    if (primaryContact.getEmail() != null) {
                        primaryContact.getEmail().setPerson(primaryContact);
                    }
                    customer.setPrimaryContact(primaryContact);
                }

            }

            if (customer.getCustomerId() > 0) {
                gojiCustomerDAO.update(gojiCustomerDAO.hydrate(customer));
            } else {
                gojiCustomerDAO.persist(customer);
            }
            return customer;
        });
        final Customer c = findExistingCustomer(cust);
        Assert.assertNotNull(c);
        Assert.assertNotNull(c.getPrimaryContact());
        Assert.assertEquals("Manhollan", c.getPrimaryContact().getLastName());

    }

    private Customer findExistingCustomer(Customer customer) {
        return gojiCustomerDAO.findUniqueByExternalId(ExternalSystemCode.EPIC_SYSTEM_CODE, customer.getExternalIds().iterator().next().getIdValue());

    }

    public void testLine() {

    }

}
