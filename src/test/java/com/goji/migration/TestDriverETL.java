/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.goji.orm.model.epic.EpicClient;
import com.goji.orm.model.epic.EpicLine;
import com.goji.orm.spring.converter.Converter;
import com.goji.orm.spring.dao.CarrierDAO;
import com.goji.orm.spring.dao.CustomerDAO;
import com.goji.orm.spring.dao.DAO;
import com.goji.orm.spring.dao.EpicClientDAO;
import com.goji.orm.spring.dao.EpicLineDAO;
import com.goji.orm.spring.dao.ExternalSystemDAO;
import com.goji.orm.spring.dao.PersonDAO;
import com.goji.orm.spring.dao.PolicyDAO;
import com.goji.orm.spring.dao.PolicyTypeDAO;
import com.goji.orm.spring.dao.StateProvinceDAO;
import com.goji.orm.spring.domain.Customer;
import com.goji.orm.spring.domain.CustomerExternalId;
import com.goji.orm.spring.domain.Policy;
import io.dropwizard.logging.BootstrapLogging;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        loader = AnnotationConfigContextLoader.class,
        classes = TestConfig.class
)
@ActiveProfiles("local")
public class TestDriverETL {

    @Autowired
    PolicyDAO policyDAO;

    @Autowired
    CustomerDAO customerDAO;

    @Autowired
    PersonDAO personDAO;

    @Autowired
    EpicClientDAO clientDAO;

    @Autowired
    EpicLineDAO lineDAO;

    @Autowired
    DAO<CustomerExternalId> customerExternalIdDAO;

    @Autowired
    private Converter<EpicClient, Customer> customerConverter;

    @Autowired
    private Converter<EpicLine, Policy> policyConverter;

    @Autowired
    CarrierDAO carrierDAO;

    @Autowired
    StateProvinceDAO stateDAO;

    @Autowired
    ExternalSystemDAO externalSystemDAO;

    @Autowired
    PolicyTypeDAO policyTypeDAO;

    @Autowired
    private TransactionTemplate tt;

    Logger logger = LoggerFactory.getLogger(TestDriverETL.class);

    @BeforeClass
    public static void beforeClass() {

        BootstrapLogging.bootstrap(Level.INFO);
        ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger("com.goji").setLevel(Level.TRACE);
    }

//    @Test
    public void testMergePolicyDrivers() {
        createOrUpdateTestCustomer();
        createOrUpdateTestPolicy();
//        List<Serializable> epicClientIds = Arrays.asList(
//                98720, 104125, 106474, 110828, 145842, 147872, 436005, 436297, 436495, 437965
//        );
//        List<Serializable> epicLineIds = Arrays.asList(
//                892106
//        //                ,
//        //                
//        //                585,
//        //                885817, 4751, 887844, 213870, 887174, 137807, 892068, 133224,
//        //                190077, 381959, 512934, 743075, 818595, 894067, 222080, 484684, 874353, 221402,
//        //                873680, 210513
//        );

    }

    private void createOrUpdateTestPolicy() {
        tt.execute(t -> {
            EpicLine l = lineDAO.get("892106");

            Policy policy = policyConverter.convert(l);
            policy = policyDAO.update(policyDAO.hydrate(policy));

            if (policy.getCustomer() != null && policy.getCustomer().getCustomerId() > 0) {
                Customer policyCustomer = policy.getCustomer();
                if (policy.getVehicles() != null && !policy.getVehicles().isEmpty()) {
                    policyCustomer.getVehicles().addAll(policy.getVehicles());
                }
                if (policy.getDrivers() != null && !policy.getDrivers().isEmpty()) {
                    policyCustomer.getDrivers().addAll(policy.getDrivers());
                }

                if (policy.getOriginatingLead() != null && policyCustomer.getOriginatingLead() == null) {
                    policyCustomer.setOriginatingLead(policy.getOriginatingLead());
                }
//                    gojiCustomerDAO.update(gojiCustomerDAO.hydrate(policyCustomer));
            }
            return policy;
        });
    }

    private void createOrUpdateTestCustomer() {
        tt.execute(t -> {

            EpicClient client = clientDAO.get("436005");
            Customer c = customerConverter.convert(client);
            return customerDAO.update(customerDAO.hydrate(c));
        });
    }
    
    @Test
    public void testThisEmptyMethod(){
        
    }

}
