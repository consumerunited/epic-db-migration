package com.goji;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.hmsonline.dropwizard.spring.SpringService;
import com.hmsonline.dropwizard.spring.SpringServiceConfiguration;
import io.dropwizard.logging.BootstrapLogging;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

public class Service extends SpringService<SpringServiceConfiguration> {

    static boolean logginInited = false;

    @Override
    public void initialize(Bootstrap<SpringServiceConfiguration> bootstrap) {
//        bootstrap.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
//        bootstrap.addBundle(new AssetsBundle("/webapp", "/swagger/", "index.html"));
        super.initialize(bootstrap);
    }

    @Override
    public void run(SpringServiceConfiguration configuration, Environment environment) throws ClassNotFoundException {
//        environment.jersey().setUrlPattern("/api/*");
//        environment.jersey().register(new SwaggerSerializers());
//        environment.jersey().register(new ApiListingResource());
//        
//        
//        environment.servlets().addServlet("bootstrap", new BootstrapServlet()).setLoadOnStartup(1);
//        final Registration.Dynamic jerseyConfig = environment.servlets().addServlet("jerseyConfig", new JerseyJaxrsConfig());
//        jerseyConfig.setInitParameter("api.version", "1.0.0");
//        jerseyConfig.setInitParameter("swagger.api.basepath", "http://localhost:8080");
//        jerseyConfig.setInitParameter("scan.all.resources", "true");

        super.run(configuration, environment);
    }

    public static void main(String[] args) throws Exception {

        final Service service = new Service();
        Service.logginInited = true;

        if (args.length == 0 || args[0].equalsIgnoreCase("localmysql")) {
            service.runWithClasspathConfig("localmysql.yml");
        } else if (args[0].equalsIgnoreCase("stage-migration")) {
            service.runWithClasspathConfig("stage.yml");
        } else if (args[0].equalsIgnoreCase("dev-migration") || args[0].equalsIgnoreCase("test-migration")) {
            service.runWithClasspathConfig("test.yml");
        } else if (args[0].equalsIgnoreCase("local")) {
            service.runWithClasspathConfig("local.yml");
        } else if (args[0].equalsIgnoreCase("test")) {
            service.runWithClasspathConfig("test.yml");
        } else if (args[0].equalsIgnoreCase("stage")) {
            service.runWithClasspathConfig("stage.yml");
        } else if (args[0].equalsIgnoreCase("prod")|| args[0].equalsIgnoreCase("prod-migration")) {
            service.runWithClasspathConfig("prod.yml");
        } else {
            service.run(args);
        }
    }

    public static void setupLogging() {
        if (!logginInited) {
            BootstrapLogging.bootstrap();
            logginInited = true;
        }
        ((LoggerContext) LoggerFactory.getILoggerFactory())
                .getLogger("com.sun.xml.internal.ws.policy.EffectiveAlternativeSelector")
                .setLevel(Level.OFF);
        Logger.getLogger("com.sun.xml.internal.ws.wspolicy.EffectiveAlternativeSelector").setLevel(java.util.logging.Level.OFF);
    }

}
