package com.goji.migration.dao.init;

import com.goji.orm.spring.StartupBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * This bean only runs in the local profile and is designed to allow for data
 * inserts into the database for the local development instance
 *
 * @author Yinzara
 */
@Component
@Profile({"local", "localmysql"})
public class LocalInit extends StartupBean {

    @Override
    @Transactional
    public void onStart() {
        
    }

}
