package com.goji.migration;

import com.goji.orm.model.epic.EpicClient;
import com.goji.orm.spring.converter.Converter;
import com.goji.orm.spring.dao.CustomerDAO;
import com.goji.orm.spring.dao.ETLStateDAO;
import com.goji.orm.spring.dao.EpicClientDAO;
import com.goji.orm.spring.domain.Customer;
import com.goji.orm.spring.domain.etl.ETLState;
import com.goji.orm.spring.domain.etl.ETLState.ETLStatusType;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.hibernate.Hibernate;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author pcm
 */
@Service
public class SyncCustomerTask {

    @Autowired
    private EpicClientDAO clientDAO;

    @Autowired
    private ETLStateDAO etlStateDAO;

    @Autowired
    private TransactionTemplate transactionTemplate;

//    @Autowired
//    private SyncEpicClient syncEpicClient;

    private Integer countFrom = 0;
    private ETLState etlStateStart;
    private DateTime lastETLTimestamp;
    private DateTime currentETLTimestamp;
    private static final int CLIENT_FETCH_SIZE = 3_000;
    private static final String ETL_TABLE_NAME = "Client";
    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SyncCustomerTask.class);

    @Scheduled(fixedDelay = 10)
    public void syncCustomer() {
        if (countFrom == 0) {
            transactionTemplate.execute((t) -> {
                currentETLTimestamp = DateTime.now();
                ETLState lastETLState = etlStateDAO.findLastFinishedRecord(ETL_TABLE_NAME);
                if (lastETLState != null) {
//                    System.out.println("Last finished ETL with " + ETL_TABLE_NAME + " had timestamp: " + lastETLState.getTimestamp().toString() + "\t Current:" + currentETLTimestamp.toString());
                    lastETLTimestamp = lastETLState.getTimestamp();
                } else {
                    lastETLTimestamp = null;
                }
                return null;
            });

        }
        final long time1 = System.nanoTime();
        List<EpicClient> epicClients = transactionTemplate.execute((t) -> {

            List<EpicClient> clients
//                                           =  clientDAO.get(Arrays.asList(379242, 381503, 393736)); 
                    = clientDAO.getAllModifiedBetween(CLIENT_FETCH_SIZE * countFrom, CLIENT_FETCH_SIZE, lastETLTimestamp, currentETLTimestamp);
            clients.forEach(c -> {
                if (c.getLines() != null) {
                    Hibernate.initialize(c.getLines());
                }
            });
            return clients;
        });
        if (epicClients.size() > 0) {
            if (countFrom == 0) {
                etlStateStart = transactionTemplate.execute((t) -> {
                    return saveETLState(UUID.randomUUID().toString(), ETLStatusType.STARTED);
                });
            }
            countFrom++;
//            Collection<Future<Void>> futures = new ArrayList<>();
            epicClients.forEach(ec
                    -> //                    futures.add(
//                    syncEpicClient.
                            processIndividualEpicClient(ec, countFrom)
            );

//            futures.stream().forEach((future) -> {
//                try {
//                    future.get();
//                } catch (InterruptedException | ExecutionException ex) {
//                    LOGGER.error("Exception with async execution..", ex);
//                }
//            });
            final long time2 = System.nanoTime();

            LOGGER.info("{}. ##### Time taken to fetch and trigger {} (fetchsize: {}) was: {} milliseconds", countFrom, epicClients.size(), CLIENT_FETCH_SIZE, (time2 - time1) / 1000000);

        } else if (countFrom > 0) {
            transactionTemplate.execute((t) -> {
                return saveETLState(etlStateStart.getUUID(), ETLStatusType.FINISHED);
            });
            countFrom = 0;
        }

    }

    private ETLState saveETLState(String uuid, ETLStatusType status) {
        ETLState s = new ETLState();
        s.setUUID(uuid);
        s.setTableName(ETL_TABLE_NAME);
        s.setStatus(status);
        s.setTimestamp(DateTime.now());
        etlStateDAO.persist(s);

        return s;
    }

//    @Component
//    public static class SyncEpicClient {

        @Autowired
        private CustomerDAO gojiCustomerDAO;

        @Autowired
        private Converter<EpicClient, Customer> customerConverter;
        
//        @Autowired
//        private TransactionTemplate transactionTemplate;

        
        @Async
        public void processIndividualEpicClient(EpicClient ec, int countFrom) {
            transactionTemplate.execute((t) -> {
                Customer c = customerConverter.convert(ec);
//            getInfoOnExistingCustomer(c);
                createOrUpdateCustomer(c, countFrom);
                return null;
            });
//            return new AsyncResult<>(null);
        }

        private void createOrUpdateCustomer(Customer customer, int countFrom) {
            if (customer != null) {
//            System.out.print(countFrom + ". Processing Customer:\t" + customer.getName()); // + "\t\t---- PrimaryContact:" + customer.getPrimaryContact().getPersonId());

                gojiCustomerDAO.hydrate(customer);
                customer = gojiCustomerDAO.update(customer);

                LOGGER.info("{}. Finished processing:\t{}\t{}", countFrom, customer.getName(), (customer.getLeads() != null && !customer.getLeads().isEmpty() ? " with Lead: " + customer.getLeads().iterator().next().getName() : "")
                );
            }
        }
//    }
}
