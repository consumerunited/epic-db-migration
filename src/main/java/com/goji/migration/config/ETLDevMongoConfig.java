/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.config;

import com.goji.mongo.spring.config.DevMongoConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@Configuration
@Profile(value = {"dev-migration", "test-migration"})
public class ETLDevMongoConfig extends DevMongoConfig {

}
