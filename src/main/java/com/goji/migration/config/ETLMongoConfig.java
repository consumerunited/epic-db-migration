/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.config;

import com.goji.mongo.spring.config.BaseMongoConfig;
import com.goji.orm.spring.value.ExternalSystemCode;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@Configuration
public class ETLMongoConfig extends BaseMongoConfig {

    @Override
    public String getSourceExternalSystem() {
        return ExternalSystemCode.ETL_SYSTEM_CODE;
    }

}
