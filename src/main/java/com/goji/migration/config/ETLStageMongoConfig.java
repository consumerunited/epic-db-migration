/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.config;

import com.goji.mongo.spring.config.StageMongoConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@Configuration
@Profile("stage-migration")
public class ETLStageMongoConfig extends StageMongoConfig{
    
}
