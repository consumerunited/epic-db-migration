/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.config;

import com.goji.mongo.spring.config.ProdMongoConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@Configuration
@Profile("prod-migration")
public class ETLProdMongoConfig extends ProdMongoConfig{
    
}
