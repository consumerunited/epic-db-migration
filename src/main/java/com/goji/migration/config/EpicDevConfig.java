/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.config;

import com.goji.orm.connections.ConnectionInfo;
import com.goji.orm.spring.config.StageRepositoryConfig;
import com.goji.orm.spring.config.TestRepositoryConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@Configuration
@Profile({"test-migration","dev-migration"})
public class EpicDevConfig extends TestRepositoryConfig{
    
    @Override
    protected ConnectionInfo internalConnectionInfo() {
        return new ConnectionInfo(
                "jdbc:mysql://internal-db-rr0.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com/consumer_united",
                "root",
                "Arlington977",
                "org.hibernate.dialect.MySQLDialect",
                "com.mysql.jdbc.Driver"
        );
    }
    
    @Override
    protected ConnectionInfo policyManagementConnectionInfo() {
         return new ConnectionInfo(
                "jdbc:mysql://internal-db-rr0.cpzuqtj4hq4e.us-east-1.rds.amazonaws.com/policy_management",
                "root",
                "Arlington977",
                "org.hibernate.dialect.MySQLDialect",
                "com.mysql.jdbc.Driver"
        );
    }
    
}
