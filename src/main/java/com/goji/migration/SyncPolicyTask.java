package com.goji.migration;

import com.goji.orm.model.Qa;
import com.goji.orm.model.epic.EpicLine;
import com.goji.orm.spring.converter.Converter;
import com.goji.orm.spring.dao.CustomerDAO;
import com.goji.orm.spring.dao.ETLStateDAO;
import com.goji.orm.spring.dao.EmployeeDAO;
import com.goji.orm.spring.dao.EpicLineDAO;
import com.goji.orm.spring.dao.PolicyDAO;
import com.goji.orm.spring.dao.PolicyTypeDAO;
import com.goji.orm.spring.dao.QaDAO;
import com.goji.orm.spring.domain.Customer;
import com.goji.orm.spring.domain.Policy;
import com.goji.orm.spring.domain.etl.ETLState;
import com.goji.orm.spring.domain.etl.ETLState.ETLStatusType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.hibernate.Hibernate;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author pcm
 */
@Service
public class SyncPolicyTask {

    @Autowired
    private EpicLineDAO lineDAO;

//    @Autowired
//    private PolicyDAO gojiPolicyDAO;

    @Autowired
    private CustomerDAO gojiCustomerDAO;

    @Autowired
    private ETLStateDAO etlStateDAO;

    @Autowired
    private TransactionTemplate transactionTemplate;

//    @Autowired
//    private SyncEpicLine syncEpicLine;


    Integer countFrom = 0;
    private ETLState etlStateStart;
    private DateTime lastETLTimestamp;
    private DateTime currentETLTimestamp;
    private static final int LINE_FETCH_SIZE = 1000;
    private static final String ETL_TABLE_NAME = "Line";
    private long counter = 0;

    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SyncPolicyTask.class);

    @Scheduled(initialDelay = 1000*60, fixedDelay = 50)
    public void syncEpicLines() {

        if (countFrom == 0 || currentETLTimestamp == null) {

            if (countFrom == 0 && currentETLTimestamp == null && lastETLTimestamp == null) {
                final Long rowCount = gojiCustomerDAO.getTotalRowCount();
                if (rowCount < 50_000) {
                    if (counter % 100 == 0) {
                        LOGGER.info("---- Waiting for the customer records size to reach at least 50,000 ----> {}", rowCount);

                    }
                    counter++;
                    return;
                } else {
                    LOGGER.info(" ---- Customer records size  is -----> {}", rowCount);

                }
            }

            transactionTemplate.execute((TransactionStatus t) -> {
                currentETLTimestamp = DateTime.now();
                ETLState lastETLState = etlStateDAO.findLastFinishedRecord(ETL_TABLE_NAME);
                if (lastETLState != null) {
//                    LOGGER.info("Last finished ETL with {} Line had timestamp: {}\t Current:{}",ETL_TABLE_NAME, lastETLState.getTimestamp().toString(), currentETLTimestamp.toString());
                    lastETLTimestamp = lastETLState.getTimestamp();

                } else {
                    lastETLTimestamp = null;

                }
                return null;
            });
        }
        final long time1 = System.nanoTime();
//        AtomicBoolean removedLines = new AtomicBoolean(false);

        List<EpicLine> epicLines
                = transactionTemplate.execute((t) -> {
                    return lineDAO.withFetchProfiles(() -> {
//                        List<EpicLine> lines= lineDAO.get(Arrays.asList
//        (694630, 694631, 698842, 698854, 727286, 731794, 731795, 731796, 731797, 839452, 850578, 880970, 885351, 885355)
//Steve 
//(727286,731794,731795,731796,731797,880970,885351,885355)
//                                );
                        List<EpicLine> lines = lineDAO.getAllActiveLinesModifiedBetween(countFrom * LINE_FETCH_SIZE, LINE_FETCH_SIZE, lastETLTimestamp, currentETLTimestamp);
//                        final long timeFetch = System.nanoTime();

                        if (lines != null) {
//                            LOGGER.info( "##### {0}. Time taken to just fetch   {1} Line records:{2} milliseconds; totalLinesReturned :{3}; ", new Object[]{countFrom, LINE_FETCH_SIZE, (timeFetch - time1) / 1000000, lines.size()});
                            lines.forEach(l -> {
                                if (l.getLineImages() != null) {
                                    Hibernate.initialize(l.getLineImages());
                                }
//                                if (l.getPolicy() != null && l.getPolicy().getLines() != null) {
//                                    Hibernate.initialize(l.getPolicy().getLines());
//                                }
                                if (l.getLineImages() != null) {
                                    l.getLineImages().forEach(i -> {
                                        if (i.getPaImages() != null) {
                                            Hibernate.initialize(i.getPaImages());

                                            i.getPaImages().forEach(pi -> {
                                                if (pi.getDrivers() != null) {
                                                    Hibernate.initialize(pi.getDrivers());
                                                }
                                                if (pi.getVehicles() != null) {
                                                    Hibernate.initialize(pi.getVehicles());
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
//                        final long timeInitialize = System.nanoTime();

//                        LOGGER.info( "##### {0}. Time taken to just initialize   {1} Line records:{2} milliseconds; totalLinesReturned :{3}; ", new Object[]{countFrom, LINE_FETCH_SIZE, (timeInitialize - timeFetch) / 1000000, lines.size()});
                        return lines;
                    }, EpicLine.LINE_WITH_COLLECTIONS);

                });
        final int totalLinesReturned = epicLines.size();

//        if (lastETLTimestamp == null && totalLinesReturned > 0) {
//            epicLines.removeIf(line
//                    -> policyExternalIdDAO.findExternalId("EPIC", String.valueOf(line.getUniqLineId())) != null
//            //                                    -> gojiPolicyDAO.findFirstByExternalId("EPIC", String.valueOf(line.getUniqLineId())) != null
//            );
//            removedLines.set(true);
//        }
        if (totalLinesReturned > 0) {

            if (countFrom == 0) {
                etlStateStart = transactionTemplate.execute((t) -> {
                    return saveETLState(UUID.randomUUID().toString(), ETLStatusType.STARTED);
                });
            }
            countFrom++;

//            epicLines.stream().filter(l -> Collections.frequency(epicLines, l) > 1).collect(Collectors.toSet()).forEach(dupLine -> System.out.println(countFrom + ". ##### Possible Duplicates for: " + dupLine.getPolicy().getPolicyNumber() + "-" + dupLine.getIssuingCompany().getCarrierCode() + "-" + dupLine.getPolicyType().getPolicyTypeCode()));
//            Collection<Future<Void>> futures = new ArrayList<>();
//            epicLines.forEach(l -> futures.add(syncEpicLine.processEpicLine(l, countFrom)));
//            futures.stream().forEach((future) -> {
//                try {
//                    future.get();
//                } catch (InterruptedException | ExecutionException ex) {
//                    LOGGER.error("Exception with async execution..", ex);
//                }
//            });
            epicLines.forEach(l -> 
//                    syncEpicLine.
                    processEpicLine(l, countFrom));

            final long time2 = System.nanoTime();
            LOGGER.info("{}. ##### Time taken to fetch & trigger  {} Line records:{} milliseconds; totalLinesReturned :{}; ", countFrom, LINE_FETCH_SIZE, (time2 - time1) / 1000000, totalLinesReturned);

        } else if (countFrom > 0) {

            transactionTemplate.execute((t) -> {
                return saveETLState(etlStateStart.getUUID(), ETLStatusType.FINISHED);
            });
//            syncEpicLine.
                    processMultiPolicy();
            countFrom = 0;
        }

    }

    private ETLState saveETLState(String uuid, ETLStatusType status) {
        ETLState s = new ETLState();
        s.setUUID(uuid);
        s.setTableName(ETL_TABLE_NAME);
        s.setStatus(status);
        s.setTimestamp(DateTime.now());
        etlStateDAO.persist(s);

        return s;
    }
//
//    @Component
//    public static class SyncEpicLine {

        @Autowired
        private Converter<EpicLine, Policy> lineConverter;

//        @Autowired
//        private TransactionTemplate transactionTemplate;
//        @Autowired
//        private CustomerDAO gojiCustomerDAO;

        @Autowired
        private PolicyDAO gojiPolicyDAO;


        @Autowired
        private QaDAO qaDAO;

        @Autowired
        private EmployeeDAO employeeDAO;

        @Autowired
        private PolicyTypeDAO gojiPolicyTypeDAO;
//        private int asyncCount;
//
//        public void setAsyncCount(Integer asyncCount) {
//            this.asyncCount = asyncCount;
//        }

        @Async
        public void processEpicLine(EpicLine l, int asyncCount) {

//            final long time3 = System.nanoTime();
            //public Future<Void> processEpicLine(EpicLine l) {
            transactionTemplate.execute((t) -> {

                Policy p = lineConverter.convert(l);
                createOrUpdatePolicy(p, asyncCount);

                return null;
            });
//            return new AsyncResult<>(null);
        }

        private void createOrUpdatePolicy(Policy policy, int asyncCount) {
            if (policy != null && policy.getPolicyNumber() != null && policy.getIssuingState() != null) {
                LOGGER.trace("{}. Processing Policy:{}({})", asyncCount, policy.getName(), policy.getPolicyId());

                gojiPolicyDAO.hydrate(policy);

                if (policy.getCustomer() != null) {
                    Customer hydratedCustomer = policy.getCustomer();
                    if (hydratedCustomer == null || hydratedCustomer.getCustomerId() == 0 || hydratedCustomer.getPrimaryContact() == null) {
                        policy.setCustomer(null);
                    }
                }
                getAgentInfo(policy);
                policy = gojiPolicyDAO.update(policy);

                if (policy.getCustomer() != null && policy.getCustomer().getCustomerId() > 0) {
                    Customer policyCustomer = policy.getCustomer();
                    if (policy.getVehicles() != null && !policy.getVehicles().isEmpty()) {
                        policyCustomer.getVehicles().addAll(policy.getVehicles());
                    }
                    if (policy.getDrivers() != null && !policy.getDrivers().isEmpty()) {
                        policyCustomer.getDrivers().addAll(policy.getDrivers());
                    }

                    if (policy.getOriginatingLead() != null && policyCustomer.getOriginatingLead() == null) {
                        policyCustomer.setOriginatingLead(policy.getOriginatingLead());
                    }
//                    gojiCustomerDAO.update(gojiCustomerDAO.hydrate(policyCustomer));
                }

                LOGGER.info("\t->{}. Finished processing :{}({}){}", asyncCount, policy.getName(), policy.getPolicyId(), policy.getCustomer() != null && policy.getCustomer().getPrimaryContact() != null ? " with : " + policy.getCustomer().getName() : " - No Customer attached");
            }
        }

        private void getAgentInfo(Policy p) {
            if (p.getCreditedAgent() == null) {
                List<Qa> qaList = qaDAO.findByPolicyNumber(p.getPolicyNumber());
                if (qaList != null && !qaList.isEmpty()) {
                    LOGGER.info(" \t-------- Found QA Policy for --------: {}", p.getPolicyNumber());
                    for (Qa q : qaList) {
                        if (q.getCreditedAgent() > 0) {
                            p.setCreditedAgent(employeeDAO.get(q.getCreditedAgent()));
                            return;
                        }
                    }

                } else {
                    LOGGER.warn(" \t-------- Did NOT find QA policy for ---: {}", p.getPolicyNumber());

                }
            }

        }

        @Async
        public void processMultiPolicy() {

            LOGGER.info("--------Processing Multi Policies--------");

            transactionTemplate.execute((t) -> {
                List<Policy> orphanPolicies = gojiPolicyDAO.findMultiPoliciesWithoutParent();
                orphanPolicies.forEach(op -> {
                    LOGGER.warn("\tCreating/Updating the parent policy for: \t{0}", op.getName());
//                List<Policy> siblings = orphanPolicies.stream().filter(q-> q.getPolicyNumber().equals(p.getPolicyNumber())).collect(Collectors.toList());
                    Policy parent = new Policy();
                    parent.setType(gojiPolicyTypeDAO.get("MULT"));
                    if (op.getPremium() != null && op.getPremium().compareTo(BigDecimal.ONE) > 0) {
                        parent.setPremium(op.getPremium());
                    }
                    parent.setPolicyNumber(op.getPolicyNumber());
//                parent.setBillToAddress(op.getBillToAddress()); -- Not doing this as the bill to address is sometimes different for individual lines.
                    parent.setCustomer(op.getCustomer());
                    parent.setEffectiveDate(op.getEffectiveDate());
                    parent.setExpirationDate(op.getExpirationDate());
                    parent.setCarrier(op.getCarrier());
                    parent.setDrivers(op.getDrivers());
                    parent.setIssuingState(op.getIssuingState());
                    parent.setFirstEffectiveDate(op.getFirstEffectiveDate());
                    parent.setStatus(op.getStatus());
                    parent.setTerm(op.getTerm());
                    parent.setTerminationDate(op.getTerminationDate());
                    parent.setTier(op.getTier());
                    parent.setVehicles(op.getVehicles());

                    parent = gojiPolicyDAO.update(gojiPolicyDAO.hydrate(parent));

                    op.setParentPolicy(parent);
                    gojiPolicyDAO.update(gojiPolicyDAO.hydrate(op));

                });

                return null;
            });

            LOGGER.info("------Done Processing Multi Policies-----");
        }

//    }

}
