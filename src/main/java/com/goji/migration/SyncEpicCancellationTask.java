package com.goji.migration;

import com.goji.orm.model.epic.EpicCancellation;
import com.goji.orm.spring.dao.CarrierDAO;
import com.goji.orm.spring.dao.ETLStateDAO;
import com.goji.orm.spring.dao.EpicCancellationDAO;
import com.goji.orm.spring.dao.PolicyDAO;
import com.goji.orm.spring.domain.Policy;
import com.goji.orm.spring.domain.etl.ETLState;
import com.goji.orm.spring.domain.etl.ETLState.ETLStatusType;
import com.goji.orm.spring.value.PolicyStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author pcm
 */
//@Service -- not in service
public class SyncEpicCancellationTask {

    @Autowired
    EpicCancellationDAO cancellationDAO;

    @Autowired
    PolicyDAO gojiPolicyDAO;

    @Autowired
    CarrierDAO carrierDAO;

    @Autowired
    ETLStateDAO etlStateDAO;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private Integer countFrom = 0;
    private ETLState etlStateStart;
    private DateTime lastETLTimestamp;
    private DateTime currentETLTimestamp;
    private static final int CANCELLATION_FETCH_SIZE = 100;

    @Scheduled(fixedDelay = 2_000)
    public void syncCustomer() {
        if (countFrom == 0) {
            transactionTemplate.execute((t) -> {
                currentETLTimestamp = DateTime.now();
                ETLState lastETLState = etlStateDAO.findLastFinishedRecord("Cancellation");
                if (lastETLState != null) {
//                    System.out.println("Last finished ETL with Cancellation had timestamp: " + lastETLState.getTimestamp().toString() + "\t Current:" + currentETLTimestamp.toString());
                    lastETLTimestamp = lastETLState.getTimestamp();
                } else {
                    lastETLTimestamp = null;
                }
                return null;
            });

        }
        List<EpicCancellation> epicCancellations = transactionTemplate.execute((t) -> {
            List<EpicCancellation> cancellations = cancellationDAO.getAllModifiedBetween(CANCELLATION_FETCH_SIZE * countFrom, CANCELLATION_FETCH_SIZE, lastETLTimestamp, currentETLTimestamp);

            return cancellations;
        });
        if (epicCancellations.size() > 0) {
            if (countFrom == 0) {
                etlStateStart = transactionTemplate.execute((t) -> {
                    return saveETLState(UUID.randomUUID().toString(), ETLStatusType.STARTED);
                });
            }
            countFrom++;

            epicCancellations.forEach(ec -> processIndividualEpicCancellation(ec));
        } else if (countFrom > 0) {
            transactionTemplate.execute((t) -> {
                return saveETLState(etlStateStart.getUUID(), ETLStatusType.FINISHED);
            });
            countFrom = 0;
        }

    }

    private ETLState saveETLState(String uuid, ETLStatusType status) {
        ETLState s = new ETLState();
        s.setUUID(uuid);
        s.setTableName("Cancellation");
        s.setStatus(status);
        s.setTimestamp(currentETLTimestamp);
        etlStateDAO.persist(s);

        return s;
    }

    @Async
    public void processIndividualEpicCancellation(EpicCancellation ec) {
        transactionTemplate.execute((t) -> {
            List<Policy> toBeUpdatedPolicies = findCancelledPolicies(ec);
            if (toBeUpdatedPolicies != null) {
                toBeUpdatedPolicies.forEach(policy -> updatePolicy(policy));
            }
            return null;
        });
    }

    private void updatePolicy(Policy policy) {
        if (policy != null) {
            System.out.print(countFrom + ". Processing Policy( Cancellation ):" + policy.getName() + "(" + policy.getPolicyId() + ") Status:--" + policy.getStatus().toString());

            policy = gojiPolicyDAO.update(gojiPolicyDAO.hydrate(policy));
            System.out.println("\t->" + countFrom + ". Finished processing (Cancellation ):" + policy.getName() + "(" + policy.getPolicyId() + ")" + (policy.getCustomer() != null ? " with : " + policy.getCustomer().getName() : " - No Customer attached") + " Status:--" + policy.getStatus().toString());
        }
    }

    private List<Policy> findCancelledPolicies(EpicCancellation ec) {
        if (ec.getRewrittenPolicyNumber() != null && ec.getRewrittenCompany() != null && !ec.getRewrittenPolicyNumber().trim().isEmpty()) {
            Policy searchEntity = new Policy();
            searchEntity.setPolicyNumber(ec.getRewrittenPolicyNumber().trim());
            searchEntity.setCarrier(carrierDAO.load(ec.getRewrittenCompany().trim()));

            List<Policy> foundPolicies = gojiPolicyDAO.findLike(searchEntity);
            if (foundPolicies.isEmpty()) {
                System.out.println("Did not find any policy with - policyNumber=" + ec.getRewrittenPolicyNumber() + " and carrier=" + ec.getRewrittenCompany() + " Effective Cancel Date=" + (ec.getRewrittenEffectiveDate() != null ? ec.getRewrittenEffectiveDate().toString() : "") + " InsertDate=" + (ec.getInsertedDate() != null ? ec.getInsertedDate().toString() : "") + " -by- " + ec.getInsertedByCode());
            } else {
                List<Policy> toBeUpdatedPolicies = new ArrayList<>();
                foundPolicies.forEach(p -> {
                    if (p.getEffectiveDate().isBefore(ec.getRewrittenEffectiveDate().toLocalDate())) {
                        p.setStatus(PolicyStatus.TERMINATED);
                        p.setTerminationDate(ec.getRewrittenEffectiveDate().toLocalDate());
//                        Note note = new Note();
//                        note.setPolicy(p);
//                        note.setContent("Policy cancelled on " + p.getTerminationDate() + " by EPIC ETL with reason:" + getReasonDetail(ec.getReasonCode()));
//                        p.setNotes(Arrays.asList(note));

                        toBeUpdatedPolicies.add(p);
                    }
                });

                return toBeUpdatedPolicies;
            }
        }
        return null;
    }

    private String getReasonDetail(String reasonCode) {
//        AC,AR,BE,CO,IR,NP,NS,NT,OT,RW,TN,UR,VN
        switch (reasonCode.trim().toUpperCase()) {
            case "AC":
                return "Agent/Company Termination";
            case "AR":
                return "Agent no longer represents the carrier";
            case "BE":
                return "";

            case "BR":
                return "Broker Of Record Change";
            case "CO":
                return "";
            case "IR":
                return "Insured's Request";
            case "NP":
                return "Non-Payment";
            case "NT":
                return "Not Taken";

            case "OB":
                return "Out Of Business";
            case "OT":
                return "Other";
            case "PR":
                return "Payment Received";
            case "RW":
                return "Rewritten";
            case "SO":
                return "Business Sold";
            case "TN":
                return "";
            case "UR":
                return "Underwriting Reasons";
            case "VN":
                return "";
            case "WC":
                return "Work Completed";
        }
        return "";
    }

}
