/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration;

import com.goji.orm.spring.dao.PolicyDAO;
import com.goji.orm.spring.domain.Policy;
import com.goji.orm.spring.value.PolicyStatus;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
@Service
public class UpdatePolicyTask {

    @Autowired
    PolicyDAO gojiPolicyDAO;

    @Autowired
    private TransactionTemplate transactionTemplate;

    //Hourly
    @Scheduled(initialDelay = 60 * 1000 * 60 * 4, fixedDelay = 60 * 1000 * 60)
    public void updatePolicies() {

        transactionTemplate.execute((t) -> {
            List<Policy> expired = gojiPolicyDAO.findExpiredActivePolicies();
            if (expired != null) {
                System.out.println("##### Total Expired Policies count: " + expired.size());
                expired.forEach(p -> {
                    if (p != null) {
                        System.out.println("Udpdating status to EXPIRED -- " + p.getName() + "\tExpiration: " + p.getExpirationDate().toString() + "\tstatus:" + p.getStatus());
                        p.setStatus(PolicyStatus.EXPIRED);
                        gojiPolicyDAO.update(p);
                    }
                });

//                gojiPolicyDAO.update(expired);
            }
            return null;
        });
    }

}
