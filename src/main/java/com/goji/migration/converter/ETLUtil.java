/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.converter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
public class ETLUtil {

    private static final Pattern ROMAN_NUMERALS = Pattern.compile("\\b(Ii|Iii|Iv|Vi|Vii|Viii)\\b");

    private static final Pattern SON_OR_DAUGTER_OF_NAMES = Pattern.compile("\\b(Mc|Nc)");

    public static String capitalizeNameFully(String name) {
        name = WordUtils.capitalizeFully(name, "-,' .".toCharArray());
        Matcher m = ROMAN_NUMERALS.matcher(name);
        if (m.find()) {//A vaild roman number is present
            final StringBuffer sb = new StringBuffer(name.length());
            do {
                m.appendReplacement(sb, m.group().toUpperCase());
            } while (m.find());
            m.appendTail(sb);
            name = sb.toString();
        }
        Matcher parentMatcher = SON_OR_DAUGTER_OF_NAMES.matcher(name);
        if (parentMatcher.find()) {
            final StringBuilder sb = new StringBuilder(name);
            do {
                if (parentMatcher.end() < name.length()) {
                    sb.replace(parentMatcher.end(), parentMatcher.end() + 1, sb.substring(parentMatcher.end(), parentMatcher.end() + 1).toUpperCase());
                }
            } while (parentMatcher.find());

            name = sb.toString();
        }

        return name;

    }
}
