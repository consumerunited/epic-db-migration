/*
 * Copyright (C) Consumer United LLC - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.goji.migration.converter;

import com.goji.common.util.PhoneUtil;
import com.goji.common.validation.DriversLicenseValidator;
import com.goji.common.value.AddressType;
import com.goji.common.value.Gender;
import com.goji.common.value.MaritalStatus;
import com.goji.common.value.PhoneType;
import com.goji.orm.model.epic.EpicClient;
import com.goji.orm.model.epic.EpicLine;
import com.goji.orm.spring.converter.Converter;
import com.goji.orm.spring.converter.CustomConverter;
import com.goji.orm.spring.dao.CarrierDAO;
import com.goji.orm.spring.dao.DAO;
import com.goji.orm.spring.dao.PolicyTypeDAO;
import com.goji.orm.spring.dao.StateProvinceDAO;
import com.goji.orm.spring.dao.VehicleMakeDAO;
import com.goji.orm.spring.dao.VehicleModelDAO;
import com.goji.orm.spring.dao.VehicleModelYearDAO;
import com.goji.orm.spring.dao.VehicleStyleDAO;
import com.goji.orm.spring.domain.AbstractPhysicalAddress;
import com.goji.orm.spring.domain.Customer;
import com.goji.orm.spring.domain.CustomerExternalId;
import com.goji.orm.spring.domain.ExternalSystem;
import com.goji.orm.spring.domain.GaragingAddress;
import com.goji.orm.spring.domain.Lead;
import com.goji.orm.spring.domain.LeadExternalId;
import com.goji.orm.spring.domain.LeadStatus;
import com.goji.orm.spring.domain.Person;
import com.goji.orm.spring.domain.PersonEmail;
import com.goji.orm.spring.domain.PersonPhone;
import com.goji.orm.spring.domain.PersonPhysicalAddress;
import com.goji.orm.spring.domain.Policy;
import com.goji.orm.spring.domain.Vehicle;
import com.goji.orm.spring.domain.VehicleMake;
import com.goji.orm.spring.domain.VehicleModel;
import com.goji.orm.spring.value.CustomerStatus;
import com.goji.orm.spring.value.ExternalSystemCode;
import com.goji.orm.spring.value.LeadState;
import com.goji.orm.spring.value.PolicyStatus;
import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

class EpicConverters {

    @Component
    static class EpicClientToGojiCustomerConverter extends CustomConverter<EpicClient, Customer> {

        @Autowired
        private StateProvinceDAO stateDAO;
//
        @Autowired
        private DAO<ExternalSystem> externalSystemDAO;

        @Override
        public Customer convert(EpicClient source, Supplier<Customer> supplier) {
            if (source.getContactNamePrimary() == null || source.getLines() == null) {
                return null;
            }
            if (!source.getLines().stream().anyMatch(l -> l.getBillModeCode().trim().equalsIgnoreCase("D"))) {
                return null;
            }

            final Customer target = new Customer();
            final Person primaryContact = new Person();
            primaryContact.setFirstName(ETLUtil.capitalizeNameFully(source.getContactNamePrimary().getFirstName()));
            primaryContact.setLastName(ETLUtil.capitalizeNameFully(source.getContactNamePrimary().getLastName()));
            primaryContact.setMiddleName(ETLUtil.capitalizeNameFully(source.getContactNamePrimary().getMiddleName()));
//            primaryContact.setDateOfBirth(new LocalDate().minusYears(100)); //-- PADriver

            if (source.getContactAddressPrimary() != null && !Strings.isNullOrEmpty(source.getContactAddressPrimary().getPostalCode()) && !Strings.isNullOrEmpty(source.getContactAddressPrimary().getStateCode()) && (ValidationData.STATE_REGEX.matcher(source.getContactAddressPrimary().getStateCode().trim()).matches())) {
                final String postalCode = source.getContactAddressPrimary().getPostalCode().trim();
                if (postalCode.length() == 5 || postalCode.length() == 9) {
                    PersonPhysicalAddress address = new PersonPhysicalAddress();

                    address.setType(AddressType.STREET);
                    address.setStreet1(source.getContactAddressPrimary().getAddress1());
                    address.setStreet2(source.getContactAddressPrimary().getAddress2() + (Strings.isNullOrEmpty(source.getContactAddressPrimary().getAddress3()) ? "" : " " + source.getContactAddressPrimary().getAddress3()));
                    if (source.getContactAddressPrimary().getCity() != null) {
                        final String city = source.getContactAddressPrimary().getCity().trim().toUpperCase();
                        if (!city.isEmpty() && city.length() <= 50) {
                            address.setCity(city);
                        }
                    }
                    address.setState(stateDAO.load(source.getContactAddressPrimary().getStateCode().trim()));
                    address.setZipCode(postalCode.length() == 5 ? postalCode : (postalCode.substring(0, 5) + "-" + postalCode.substring(5)));

                    List<PersonPhysicalAddress> addressList = new ArrayList<>();
                    addressList.add(address);
                    primaryContact.setAddresses(addressList);
                }
            }
            if (source.getContactNamePrimary() != null && source.getContactNamePrimary().getEmailMain() != null && ValidationData.isValidEmail(source.getContactNamePrimary().getEmailMain().getEmailWeb())) {
                primaryContact.setEmail(new PersonEmail(primaryContact, source.getContactNamePrimary().getEmailMain().getEmailWeb().trim()));

            }
            if (source.getContactNumberPrimary() != null) {

//                if (source.getContactNumberPrimary().getTypeCode().toUpperCase().equals("EM1") || source.getContactNumberPrimary().getTypeCode().toUpperCase().equals("EM2")) {
//                    primaryContact.setEmail(new PersonEmail(primaryContact, source.getContactNumberPrimary().getEmailWeb()));
//                }
                if (!Strings.isNullOrEmpty(source.getContactNumberPrimary().getNumber())) {
                    setPhone(primaryContact, source.getContactNumberPrimary().getNumber(), source.getContactNumberPrimary().getTypeCode().toUpperCase());
                } else {
                    setPhone(primaryContact, source.getContactNamePrimary().getContactNumberMain().getNumber(), source.getContactNumberPrimary().getTypeCode().toUpperCase());
                }
            }

            target.setPrimaryContact(primaryContact);
            Set<CustomerExternalId> externalIds = new HashSet<>();
            externalIds.add(new CustomerExternalId(ExternalSystemCode.EPIC_SYSTEM_CODE, String.valueOf(source.getUniqEntityId())));
            target.setExternalIds(externalIds);
            target.setStatus(CustomerStatus.SOLD);
            if (source.getConversionPriorAccountID() != null && source.getConversionPriorAccountID().trim().length() > 1) {
                Lead originatingLead = new Lead();
                originatingLead.setExternalIds(ImmutableSet.of(new LeadExternalId(ExternalSystemCode.INTERNAL_SYSTEM_CODE, source.getConversionPriorAccountID().trim())));
                originatingLead.setCustomer(target);
                originatingLead.setStatus(new LeadStatus());
                originatingLead.getStatus().setLeadState(LeadState.CONVERTED);
                target.setOriginatingLead(originatingLead);
            }

//            Set<Policy> customerPolicies = new HashSet();
//            source.getLines().stream().filter(l -> l.getBillModeCode().trim().equalsIgnoreCase("D")).forEach(l -> {
//                Policy p = new Policy();
//                if(l.getPolicy()!=null && l.getPolicy().getPolicyNumber()!=null)
//                
//                p.setExternalId(externalSystemDAO.load(ExternalSystemCode.EPIC_SYSTEM_CODE), String.valueOf(l.getUniqLineId()));
//                p.setCustomer(target);
//                customerPolicies.add(p);
//            });
//            target.setPolicies(customerPolicies);
            return target;
        }

        private void setPhone(Person contact, String phoneNumber, String phoneType) {
            if (Strings.isNullOrEmpty(phoneNumber)) {
                return;
            }
//            phoneNumber = CharMatcher.DIGIT.retainFrom(phoneNumber);
            phoneNumber = PhoneUtil.parsePhone(phoneNumber, PhoneUtil.InvalidPhoneBehavior.RETURN_NULL);

            if (phoneNumber != null && !ValidationData.BAD_PHONE_REGEX.matcher(phoneNumber).matches()) {
                PersonPhone phone = new PersonPhone();
                phone.setPhoneNumber(phoneNumber);

//            BUS,MOB,OTH,RES
                switch (phoneType) {
                    case "MOB":
                        phone.setType(PhoneType.CELL);
                        break;
                    case "BUS":
                        phone.setType(PhoneType.WORK);
                        break;
                    default:
                        phone.setType(PhoneType.HOME);

                }

                List<PersonPhone> phones = new ArrayList<>();
                phones.add(phone);

                contact.setPhones(phones);
            }
        }

    }

    @Component
    static class EpicLineToGojiPolicyConverter extends CustomConverter<EpicLine, Policy> {

        @Autowired
        private StateProvinceDAO stateDAO;

        @Autowired
        private DAO<ExternalSystem> externalSystemDAO;

        @Autowired
        private PolicyTypeDAO policyTypeDAO;

        @Autowired
        private CarrierDAO carrierDAO;

        @Autowired
        private Converter<EpicClient, Customer> customerConverter;

        @Autowired
        private VehicleModelYearDAO vehicleModelYearDAO;

        @Autowired
        private VehicleModelDAO vehicleModelDAO;

        @Autowired
        private VehicleMakeDAO vehicleMakeDAO;
//        private int testCount = 0;
        @Autowired
        VehicleStyleDAO vehicleStyleDAO;

        @Override
        public Policy convert(EpicLine source, Supplier<Policy> supplier) {

            Policy target = new Policy();

            target.setPolicyNumber(source.getPolicy().getPolicyNumber());
            target.setInceptionDate(source.getFirstWrittenDate());
            //TODO: Get The first effective date from Service summary or calculate it based on the term, effectiveDate and firstWrittenDate            
            target.setFirstEffectiveDate(source.getFirstWrittenDate());
            target.setEffectiveDate(source.getEffectiveDate());
            target.setExpirationDate(source.getExpirationDate());

            if (ValidationData.STATE_REGEX.matcher(source.getIssuingState().trim().toUpperCase()).matches()) {
                target.setIssuingState(stateDAO.load(source.getIssuingState().trim()));
            }
            if (target.getPolicyNumber() == null || target.getPolicyNumber().isEmpty() || target.getPolicyNumber().equalsIgnoreCase("DISREGARD") || target.getIssuingState() == null) {
                return null;
            }
            target.setType(policyTypeDAO.get(source.getPolicyType().getPolicyTypeCode().toUpperCase().equals("PPKG") ? "MULT" : source.getPolicyType().getPolicyTypeCode()));
            target.setCarrier(carrierDAO.load(source.getIssuingCompany().getCarrierCode()));

            if (source.getPostalCodeBillTo() != null) {
                AbstractPhysicalAddress.EmbeddablePhysicalAddress billToAddress = new AbstractPhysicalAddress.EmbeddablePhysicalAddress();
                final String postalCode = source.getPostalCodeBillTo().trim();
                if (postalCode.length() == 5 || postalCode.length() == 9) {
                    billToAddress.setZipCode(postalCode.length() == 5 ? postalCode : (postalCode.substring(0, 5) + "-" + postalCode.substring(5)));
                }
                if (source.getCdStateCodeBillTo() != null && source.getCdStateCodeBillTo().trim().length() == 2 && (ValidationData.STATE_REGEX.matcher(source.getCdStateCodeBillTo().trim()).matches())) {
                    billToAddress.setState(stateDAO.load(source.getCdStateCodeBillTo().trim()));
                }
                billToAddress.setStreet1(source.getAddress1BillTo());
                billToAddress.setStreet2(source.getAddress2BillTo() + (Strings.isNullOrEmpty(source.getAddress3BillTo()) ? "" : " " + source.getAddress3BillTo()));
                if (source.getCityBillTo() != null) {
                    final String city = source.getCityBillTo().trim().toUpperCase();
                    if (!city.isEmpty() && city.length() <= 50) {
                        billToAddress.setCity(city);
                    }
                }

                target.setBillToAddress(billToAddress);
            }
            mapDriversAndVehicles(source, target);
            mapPolicyStatus(source, target);
            target.setCustomer(getCustomerWithExternalId(source.getClient()));
            target.setPremium(source.getLastDownloadedPremium());
            target.setTerm(Months.monthsBetween(target.getEffectiveDate(), target.getExpirationDate()).compareTo(Months.EIGHT) > 0 ? Months.TWELVE : Months.SIX);

            Map<ExternalSystem, String> externalIds = new HashMap<>();
            externalIds.put(externalSystemDAO.load(ExternalSystemCode.EPIC_SYSTEM_CODE), String.valueOf(source.getUniqLineId()));
            target.setExternalIds(externalIds);
            return target;
        }

        private void mapPolicyStatus(EpicLine source, Policy target) {
            if (source.getLineStatus() != null && source.getLineStatus().getCdLineStatusCode() != null) {

                final String epicLineStatus = source.getLineStatus().getCdLineStatusCode().trim().toUpperCase();

                switch (epicLineStatus) {
                    case "CAN": //Cancelled
                        target.setStatus(PolicyStatus.TERMINATED);
                        break;

                    case "NON": //Non-Renewal
                        target.setStatus(PolicyStatus.EXPIRED);
                        break;
                    case "REN"://Renewal 

                    case "REW"://Rewrite 

                    case "RI"://Reissue 

                    case "RIN"://Re-Instatement 

                    case "NEW": //New Policy

                    case "RWQ"://Renewal 
                        if (target.getEffectiveDate().isAfter(LocalDate.now())) {
                            target.setStatus(PolicyStatus.CONFIRMED);
                        } else if (target.getExpirationDate().isBefore(LocalDate.now())) {
                            target.setStatus(PolicyStatus.EXPIRED);
                        } else {
                            target.setStatus(PolicyStatus.ACTIVE);
                        }
                        break;

                    case "EN"://Endorsement 
                    case "SYN"://Database Synchronization  
                    default:

                        break;

                }
            }

        }

        private void mapDriversAndVehicles(EpicLine source, Policy target) {
            if (source.getLineImages() != null && !source.getLineImages().isEmpty()) {
//                List<Person> drivers = new ArrayList<>();
//                List<Vehicle> vehicles = new ArrayList<>();
                source.getLineImages().stream().distinct().forEach(l -> {
                    if (l.getPaImages() != null && !l.getPaImages().isEmpty()) {
                        l.getPaImages().stream().distinct().forEach(i -> {
                            if (i.getDrivers() != null && !i.getDrivers().isEmpty()) {
                                i.getDrivers().stream().distinct().forEach(d -> {

                                    if (d.getLicenseNumber() != null && d.getLicenseNumber().trim().length() > 0) {
                                        Person person = new Person();

                                        person.setFirstName(ETLUtil.capitalizeNameFully(d.getFirstName()));
                                        person.setMiddleName(ETLUtil.capitalizeNameFully(d.getMiddleName()));
                                        person.setLastName(ETLUtil.capitalizeNameFully(d.getLastName()));
                                        if (d.getGenderCode().trim().equalsIgnoreCase("M")) {
                                            person.setGender(Gender.M);
                                        } else if (d.getGenderCode().trim().equalsIgnoreCase("F")) {
                                            person.setGender(Gender.F);

                                        }
                                        person.setDateOfBirth(new LocalDate(d.getBirthDate()));
                                        if (d.getLicensedState() != null && (ValidationData.STATE_REGEX.matcher(d.getLicensedState().trim()).matches())) {
                                            person.setLicenseState(stateDAO.load(d.getLicensedState().trim()));
                                        }
                                        if (d.getLicensedDate() != null) {
                                            person.setDateFirstLicensed(new LocalDate(d.getLicensedDate()));
                                        }

                                        if (d.getLicenseNumber() != null && person.getLicenseState() != null) {
                                            final String dlNum = CharMatcher.JAVA_LETTER_OR_DIGIT.retainFrom(d.getLicenseNumber()).toUpperCase();
                                            if (DriversLicenseValidator.getInstance().isValid(person.getLicenseState().getStateCode(), dlNum)) {

                                                person.setLicenseNumber(dlNum);
                                            }
                                        }

                                        if (d.getMaritalStatusCode() != null) {
                                            switch (d.getMaritalStatusCode().trim().toUpperCase()) {
                                                case "C": //Domestic Partner
                                                    person.setMaritalStatus(MaritalStatus.PARTNERED);
                                                    break;
                                                case "D":// Divorced
                                                    person.setMaritalStatus(MaritalStatus.DIVORCED);
                                                    break;
                                                case "M": // Married
                                                    person.setMaritalStatus(MaritalStatus.MARRIED);
                                                    break;
                                                case "P": //Separated
                                                    person.setMaritalStatus(MaritalStatus.SEPARATED);
                                                    break;
                                                case "S": //Single
                                                    person.setMaritalStatus(MaritalStatus.SINGLE);
                                                    break;
                                                case "W":// Widowed
                                                    person.setMaritalStatus(MaritalStatus.WIDOWED);
                                                    break;
                                                case "V"://CivilUnionRegisteredDomesticPartner
                                                    break;
                                                case "U": //Unknown
                                                    break;
                                                case "O": //Other
                                                    break;
                                            }
                                        }
                                        if (target.getDrivers() == null || target.getDrivers().isEmpty() || !isDuplicateDriver(target.getDrivers(), person)) {
//                                            System.out.println("Adding:" + person.getName() + " to " + target.getName());
                                            target.addDriver(person);
//                                            drivers.add(person);
                                        }
                                    }
                                });
                            }

                            if (i.getVehicles() != null && !i.getVehicles().isEmpty()) {
                                i.getVehicles().stream().distinct().forEach(v -> {
                                    Vehicle vehicle = new Vehicle();
                                    vehicle.setDescription(v.getVehicleYear() + " " + v.getMake() + " " + v.getModel());
                                    if (v.getMake() != null && v.getVehicleYear() != null) {
                                        VehicleMake make = vehicleMakeDAO.getByNaturalId(v.getMake().trim().toLowerCase());
                                        if (make != null && v.getModel() != null) {
                                            VehicleModel model = vehicleModelDAO.findByNameAndMake(v.getModel().trim(), make);
                                            if (model != null) {
                                                vehicle.setModelYear(vehicleModelYearDAO.findByModelAndYear(model, v.getVehicleYear()));
                                            }

                                        }
                                    }
                                    if (v.getPostalCodeGarage() != null) {
                                        GaragingAddress garagingAddress = new GaragingAddress();
                                        final String postalCodeGarage = v.getPostalCodeGarage().trim();
                                        if (postalCodeGarage.length() == 5) {
                                            garagingAddress.setZipCode(postalCodeGarage);
                                        } else if (postalCodeGarage.length() == 9) {
                                            garagingAddress.setZipCode(postalCodeGarage.substring(0, 5) + "-" + postalCodeGarage.substring(5));
                                        }
                                        garagingAddress.setStreet1(v.getAddress1Garage());
                                        garagingAddress.setStreet2(v.getAddress2Garage() + (Strings.isNullOrEmpty(v.getAddress3Garage()) ? "" : " " + v.getAddress3Garage()));
                                        if (v.getCityGarage() != null) {
                                            final String city = v.getCityGarage().trim().toUpperCase();
                                            if (!city.isEmpty() && city.length() <= 50) {
                                                garagingAddress.setCity(city);
                                            }
                                        }

                                        if (ValidationData.STATE_REGEX.matcher(v.getStateCodeGarage().trim()).matches()) {
                                            garagingAddress.setState(stateDAO.load(v.getStateCodeGarage().trim()));
                                        }
                                        vehicle.setGaragingAddress(garagingAddress);
                                    }
                                    vehicle.setEstimatedYearlyMiles(v.getAnnualMileage());
                                    if (v.getVin() != null && v.getVin().trim().length() >= vehicleStyleDAO.SQUISHVIN_LENGTH && v.getVin().trim().length() <= vehicleStyleDAO.VIN_LENGTH) {
                                        vehicle.setVin(v.getVin().trim());
                                    }

                                    if (target.getVehicles() == null || target.getVehicles().isEmpty() || !isDuplicateVehicle(target.getVehicles(), vehicle)) {
//                                        System.out.println("Adding Vehicle: " + vehicle.getVin() + " to " + target.getName());
//                                        vehicles.add(vehicle);
                                        target.addVehicle(vehicle);
                                    }
                                });

                            }
                        });
                    }
                });
//                target.setDrivers(drivers);
//                target.setVehicles(vehicles);
            }

        }

        private Customer getCustomerWithExternalId(EpicClient client) {
            Customer target = new Customer();
            Map<ExternalSystem, String> externalIds = new HashMap<>();
            externalIds.put(externalSystemDAO.load(ExternalSystemCode.EPIC_SYSTEM_CODE), String.valueOf(client.getUniqEntityId()));
            target.setExternalIds(externalIds);
            return target;
        }

        private boolean isDuplicateDriver(List<Person> drivers, Person currentDriver) {
            if (currentDriver == null) {
                return false;
            }
            if (currentDriver.getLicenseNumber() != null) {
                return drivers.stream().anyMatch(dr
                        -> (dr != null
                        && dr.getLicenseNumber() != null
                        && dr.getLicenseNumber().equalsIgnoreCase(currentDriver.getLicenseNumber())));
            } else if (currentDriver.getDateOfBirth() != null) {
                return drivers.stream().anyMatch(dr
                        -> dr != null && dr.getDateOfBirth() != null
                        && dr.getDateOfBirth().equals(currentDriver.getDateOfBirth())
                        && dr.getFirstName() != null && dr.getFirstName().equalsIgnoreCase(currentDriver.getFirstName()));
            } else {
                return drivers.stream().anyMatch(dr
                        -> dr != null && dr.getFirstName().equalsIgnoreCase(currentDriver.getFirstName())
                        && dr.getLastName().equalsIgnoreCase(currentDriver.getLastName()));
            }

        }

        private boolean isDuplicateVehicle(List<Vehicle> vehicles, Vehicle v) {

            return vehicles.stream().anyMatch(vl
                    -> (vl != null
                    && v != null
                    && vl.getVin() != null
                    && vl.getVin().equalsIgnoreCase(v.getVin())));

        }

    }

}
