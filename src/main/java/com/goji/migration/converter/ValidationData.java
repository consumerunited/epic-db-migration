/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.converter;

import java.util.regex.Pattern;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
public class ValidationData {

    public static final String STATE_REGEX_STRING = "^AL|AK|AZ|AR|CA|CO|CT|DE|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY$";
    public static final String BAD_EMAILS_STRING = "^a@a.com|a@gmail.com|info@goji.com|noemail@goji.com|a@aol.com|(hard bounce)|2@gmail.com|(bad domain)|none@none.com|noemail@gmail.com|scrooge55555@gmail.com|none@aol.com|noemail@aol.com|ac3@yahoo.com|(unknown user)|service@goji.com|abc@aol.com|service@consumerunited.com|a@b.com|unknown@aol.com|fake@fake.com|noemail@yahoo.com|none@gmail.com|a.a@yahoo.com|1@1.com|keara.hoonan@goji.com|(opt-out)|email@email.com|asdf@asdf.com|tom.mayell@consumerunited.com|no@email.com|a.a@aol.com|aol@aol.com|asdasd@gmail.com|asd@asd.com|kim.dinsmore@yahoo.com|noemail@noemail.com|ask@yahoo.com|none@goji.com|doesnothave@gmail.com|asd@gmail.com|no@inbound.com|aa@aa.com|na@na.com|noemail@email.com|no@no.com|carl.moravec@goji.com|no@name.com|b@aol.com|2@2.com|3@gmail.com|none@test.com|a.a@msn.com|aaa@a.com|aaa@aaa.com|aaa@aol.com|asdsad@gmail.com|fake@gmail.com|g@g.com|1234@test.com|123@123.com|123@456.com|123@a1234.com|1234@test.com|1234@abc.com|s@s.com|as@as.com|aa@a.com|email@aol.com|123@gmail.com|gmail@gmail.com$";
    public static final String BAD_PHONE_STRING = "^2147483647|9999999999|5555555555|1111111111|8662889868|8889911054|6174824700|6174784700|6174874700$";
    public static final String BAD_STREET1_STRING = "^main st|park ave|washington ave|central ave|washington st|lincoln ave|w main st$";

    public static final Pattern STATE_REGEX = Pattern.compile(STATE_REGEX_STRING);
    public static final Pattern BAD_EMAILS_REGEX = Pattern.compile(BAD_EMAILS_STRING);
    public static final Pattern BAD_PHONE_REGEX = Pattern.compile(BAD_PHONE_STRING);
    public static final Pattern BAD_STREET1_REGEX = Pattern.compile(BAD_STREET1_STRING);

    public static boolean isValidEmail(String email) {
        if (email == null) {
            return false;
        }
        if (!email.contains("@")) {
            return false;
        }
        email = email.trim().toLowerCase();
        if (email.length() < 6) {
            return false;
        }
        return !BAD_EMAILS_REGEX.matcher(email.trim()).matches();
    }

}
