package com.goji.migration;

import com.goji.orm.model.epic.EpicServiceSummary;
import com.goji.orm.spring.dao.CarrierDAO;
import com.goji.orm.spring.dao.ETLStateDAO;
import com.goji.orm.spring.dao.EpicServiceSummaryDAO;
import com.goji.orm.spring.dao.PolicyDAO;
import com.goji.orm.spring.dao.PolicyDispositionDAO;
import com.goji.orm.spring.dao.PolicyTypeDAO;
import com.goji.orm.spring.domain.Carrier;
import com.goji.orm.spring.domain.Policy;
import com.goji.orm.spring.domain.PolicyType;
import com.goji.orm.spring.domain.etl.ETLState;
import com.goji.orm.spring.domain.etl.ETLState.ETLStatusType;
import com.goji.orm.spring.value.ExternalSystemCode;
import com.goji.orm.spring.value.PolicyStatus;
import java.util.List;
import java.util.UUID;
import org.hibernate.Hibernate;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author pcm
 */
@Service
public class SyncEpicServiceSummaryCancellationTask {

    @Autowired
    EpicServiceSummaryDAO ssCancellationDAO;

    @Autowired
    PolicyDAO gojiPolicyDAO;

    @Autowired
    CarrierDAO gojiCarrierDAO;

    @Autowired
    private PolicyTypeDAO gojiPolicyTypeDAO;

    @Autowired
    ETLStateDAO etlStateDAO;

    @Autowired
    private TransactionTemplate transactionTemplate;
    
    @Autowired
    PolicyDispositionDAO dispositionDAO;

    private Integer countFrom = 0;
    private ETLState etlStateStart;
    private DateTime lastETLTimestamp;
    private DateTime currentETLTimestamp;
    private static final int CANCELLATION_FETCH_SIZE = 500;
    private static final String ETL_TABLE_NAME = "ServiceSummaryCancellation";

    private long counter;

    @Scheduled(fixedDelay = 200)
    public void syncCustomer() {
        if (countFrom == 0) {

            if (countFrom == 0 && currentETLTimestamp == null && lastETLTimestamp == null) {
                final Long rowCount = gojiPolicyDAO.getTotalRowCount();
                if (rowCount < 80_000) {
                    if (counter % 100 == 0) {
                        System.out.println("---- Waiting for the policy records size to reach at least 80,000 ----> " + rowCount);
                    }
                    counter++;
                    return;
                } else {
                    System.out.println(" ---- Policy records size  is -----> " + rowCount);

                }
            }
            transactionTemplate.execute((t) -> {

                currentETLTimestamp = DateTime.now();
                ETLState lastETLState = etlStateDAO.findLastFinishedRecord(ETL_TABLE_NAME);
                if (lastETLState != null) {
//                    System.out.println("Last finished ETL with " + ETL_TABLE_NAME + " had timestamp: " + lastETLState.getTimestamp().toString() + "\t Current:" + currentETLTimestamp.toString());
                    lastETLTimestamp = lastETLState.getTimestamp();
                } else {
                    lastETLTimestamp = null;
                }
                return null;
            });

        }
        List<EpicServiceSummary> epicCancellations = transactionTemplate.execute((t) -> {
            List<EpicServiceSummary> cancellations = ssCancellationDAO.getCancellationsBetween(CANCELLATION_FETCH_SIZE * countFrom, CANCELLATION_FETCH_SIZE, lastETLTimestamp, currentETLTimestamp);

            cancellations.forEach(c -> {
//                if (c.getPolicy() != null) {
//                    Hibernate.initialize(c.getPolicy().getLines());
//                }
                if (c.getLine() != null) {
                    Hibernate.initialize(c.getLine().getLineImages());
                }
                if (c.getCancellation() != null) {
                    Hibernate.initialize(c.getCancellation().getRemarks());
                }
//                Hibernate.initialize(c.getReplacesServiceSummary());
            }
            );

            return cancellations;
        });
        if (epicCancellations.size() > 0) {
            if (countFrom == 0) {
                etlStateStart = transactionTemplate.execute((t) -> {
                    return saveETLState(UUID.randomUUID().toString(), ETLStatusType.STARTED);
                });
            }
            countFrom++;

            epicCancellations.forEach(ss -> processIndividualEpicServiceSummary(ss));
        } else if (countFrom > 0) {
            transactionTemplate.execute((t) -> {
                return saveETLState(etlStateStart.getUUID(), ETLStatusType.FINISHED);
            });
            countFrom = 0;
        }

    }

    private ETLState saveETLState(String uuid, ETLStatusType status) {
        ETLState s = new ETLState();
        s.setUUID(uuid);
        s.setTableName(ETL_TABLE_NAME);
        s.setStatus(status);
        s.setTimestamp(DateTime.now());
        etlStateDAO.persist(s);

        return s;
    }

    @Async
    public void processIndividualEpicServiceSummary(EpicServiceSummary ss) {
        transactionTemplate.execute((t) -> {
            Policy toBeUpdatedPolicy = findCancelledPolicy(ss);
            updatePolicy(toBeUpdatedPolicy);
            return null;
        });
    }

    private void updatePolicy(Policy policy) {
        if (policy != null) {
            System.out.println(countFrom + ". Processing Policy( Cancellation ):" + policy.getName() + "(" + policy.getPolicyId() + ") Status:--" + policy.getStatus().toString());

            policy = gojiPolicyDAO.update(gojiPolicyDAO.hydrate(policy));
            System.out.println("\t->" + countFrom + ". Finished processing (Cancellation ):" + policy.getName() + "(" + policy.getPolicyId() + ")" + (policy.getCustomer() != null ? " with : " + policy.getCustomer().getName() : " - No Customer attached") + " Status:--" + policy.getStatus().toString());
        }
    }

    private Policy findCancelledPolicy(EpicServiceSummary ss) {
//        System.out.println(ToStringBuilder.reflectionToString(ss, ToStringStyle.MULTI_LINE_STYLE));
    if (ss.getLine() != null) {
            Policy p = gojiPolicyDAO.findUniqueByExternalId(ExternalSystemCode.EPIC_SYSTEM_CODE, String.valueOf(ss.getLine().getUniqLineId()));
            if (p == null) {
                if (ss.getLine().getPolicy() != null) {
                    final String policyNumber = ss.getLine().getPolicy().getPolicyNumber().trim();
                    final Carrier carrier = gojiCarrierDAO.load(ss.getLine().getIssuingCompany().getCarrierCode().trim());
                    final PolicyType type
                            = gojiPolicyTypeDAO.get(
                                    ss.getLine().getPolicyType().getPolicyTypeCode().trim().toUpperCase().equals("PPKG")
                                    ? "MULT"
                                    : ss.getLine().getPolicyType().getPolicyTypeCode().trim());

                    p = gojiPolicyDAO.findPolicyByNumberCarrierAndType(policyNumber, carrier, type);

                }
            }

            if (p != null) {
                p.setStatus(PolicyStatus.TERMINATED);
                
                p.setDisposition(dispositionDAO.get(ss.getCancellation().getReasonCode()));
                p.setTerminationDate(ss.getEffectiveDate().toLocalDate());
            }

            return p;
        }
        return null;
    }

//    private String getReasonDetail(String reasonCode) {
////        AC,AR,BE,CO,IR,NP,NS,NT,OT,RW,TN,UR,VN
//        switch (reasonCode.trim().toUpperCase()) {
//            case "AC":
//                return "Agent/Company Termination";
//            case "AR":
//                return "Agent no longer represents the carrier";
//            case "BE":
//                return "";
//
//            case "BR":
//                return "Broker Of Record Change";
//            case "CO":
//                return "";
//            case "IR":
//                return "Insured's Request";
//            case "NP":
//                return "Non-Payment";
//            case "NT":
//                return "Not Taken";
//
//            case "OB":
//                return "Out Of Business";
//            case "OT":
//                return "Other";
//            case "PR":
//                return "Payment Received";
//            case "RW":
//                return "Rewritten";
//            case "SO":
//                return "Business Sold";
//            case "TN":
//                return "";
//            case "UR":
//                return "Underwriting Reasons";
//            case "VN":
//                return "";
//            case "WC":
//                return "Work Completed";
//        }
//        return "";
//    }
}
