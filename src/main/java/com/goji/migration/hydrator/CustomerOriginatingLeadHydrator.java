/*
 *  Copyright (C) Consumer United LLC - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 */
package com.goji.migration.hydrator;

import com.goji.orm.model.Lead;
import com.goji.orm.spring.converter.WhichProperties;
import com.goji.orm.spring.dao.LegacyLeadDAO;
import com.goji.orm.spring.dao.hydration.CustomHydrator;
import com.goji.orm.spring.domain.Customer;
import com.goji.orm.spring.domain.LeadExternalId;
import com.goji.orm.spring.domain.LeadStatus;
import com.goji.orm.spring.value.ExternalSystemCode;
import com.goji.orm.spring.value.LeadState;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Prakash Maurya <prakash@goji.com>
 */
public class CustomerOriginatingLeadHydrator implements CustomHydrator<Customer> {

    private static final String REFERRAL_LEAD_PARTNER_ID = "26";

    @Autowired
    private LegacyLeadDAO leadDAO;

    @Override
    public boolean hydrate(Customer toHydrate, Customer existing, WhichProperties whichProperties) {
        if (toHydrate.getOriginatingLead() != null && toHydrate.getOriginatingLead().getExternalIds() != null) {
            final String internalLeadId = toHydrate.getOriginatingLead().getExternalIds().stream()
                    .filter(eid -> eid.getSystemCode().equals(ExternalSystemCode.INTERNAL_SYSTEM_CODE))
                    .map(LeadExternalId::getIdValue)
                    .findFirst().orElse(null);
            if (internalLeadId != null) {
                if (existing != null && existing.getOriginatingLead() != null) {
                    if (existing.getOriginatingLead().getExternalIds().stream().anyMatch(t -> t.getSystemCode().equals(ExternalSystemCode.INTERNAL_SYSTEM_CODE) && t.getIdValue().equals(internalLeadId))) {
                        return false;
                    }
                }
                final Lead lead = leadDAO.findByLeadId(internalLeadId);
                if (lead.getInfo() != null && lead.getInfo().getPartnerId().equals(REFERRAL_LEAD_PARTNER_ID)) {
                    //we need to see if we can find a lead matching the same information as this lead where the partner ID is not a referral partner
                    // If so, set that lead as the originating lead for this customer and return true immediately
                    Set<String> numbers = new HashSet<>();

                    if (!Strings.isNullOrEmpty(lead.getEveningHomePhone())) {
                        numbers.add(lead.getEveningHomePhone());
                    }
                    if (!Strings.isNullOrEmpty(lead.getDaytimePhone())) {
                        numbers.add(lead.getDaytimePhone());
                    }
                    if (!Strings.isNullOrEmpty(lead.getMobilePhone())) {
                        numbers.add(lead.getMobilePhone());
                    }

                    List<Lead> foundLeadsBynumber = leadDAO.findByNumbersAndName(numbers, lead.getFirstName(), lead.getLastName());

                    if (foundLeadsBynumber != null && !foundLeadsBynumber.isEmpty()) {
                        for (Lead l : foundLeadsBynumber) {
                            if (Long.valueOf(l.getLeadId()) < Long.valueOf(lead.getId())) {

                                final com.goji.orm.spring.domain.Lead newOriginatingLead = new com.goji.orm.spring.domain.Lead();
                                newOriginatingLead.setExternalIds(ImmutableSet.of(new LeadExternalId(ExternalSystemCode.INTERNAL_SYSTEM_CODE, l.getLeadId())));
                                newOriginatingLead.setCustomer(toHydrate);
                                newOriginatingLead.setStatus(new LeadStatus());
                                newOriginatingLead.getStatus().setLeadState(LeadState.CONVERTED);
                                toHydrate.setOriginatingLead(newOriginatingLead);
                                return true;
                            }
                        }
                    }

                    if (existing != null && existing.getOriginatingLead() != null) {
                        toHydrate.setOriginatingLead(existing.getOriginatingLead());

                    }
                }
            }
        }

        return false;
    }

}
